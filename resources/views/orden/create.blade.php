@extends('layouts.adminlte')
@section('content')
  <div class="container">
    <div class="col-sm-offset-3 col-sm-6">
      <div class="panel-title">
        <h1>Orden de Servicio --LINEAS AEREAS--</h1>
      </div>
      <div class="panel-body">
        <!--muestra validacion de errores-->
        @include('common.errors')

        <form action="{{route('orden.store')}}" method="POST">
          {{ csrf_field()}} <!--codigo de seguridad cuando se hacen peticiones al servidor-->

          <div class="form-group">
            <label for="linea_aerea" class="control-label">LINEA_AEREA </label>
            <select class="form-control" name="linea_aerea" >

              @foreach ($clientes as $cliente)
                <option  value="{{ $cliente->nombre }}">{{ $cliente->nombre }}</option>
              @endforeach
            </select>

          </div>

          <div class="form-group">  <!-- formulario que hace referencia a cliente-->
            <label for="tipo_de_avion" class="control-label">TIPO DE AVION</label>
            <input type="text" name="tipo_de_avion" class="form-control">
          </div>

          <div class="form-group">  <!-- formulario que hace referencia a cliente-->
            <label for="numero_de_vuelo" class="control-label">NUMERO DE VUELO</label>
            <input type="text" name="numero_de_vuelo" class="form-control">
          </div>

          <div class="form-group">  <!-- formulario que hace referencia a cliente-->
            <label for="matricula" class="control-label">MATRICULA</label>
            <input type="text" name="matricula" class="form-control">
          </div>

          <div class="form-group">  <!-- formulario que hace referencia a cliente-->
            <label for="tiempo_inicio" class="control-label">TIEMPO INICIO</label>
            <input type="text" name="tiempo_inicio" class="form-control">
          </div>

          <div class="form-group">  <!-- formulario que hace referencia a cliente-->
            <label for="tiempo_terminado" class="control-label">TIEMPO TERMINADO</label>
            <input type="text" name="tiempo_terminado" class="form-control">
          </div>

          <div class="form-group">  <!-- formulario que hace referencia a cliente-->
            <label for="tiempo_total" class="control-label">TIEMPO TOTAL</label>
            <input type="text" name="tiempo_total" class="form-control">
          </div>

          <div class="form-group">  <!-- formulario que hace referencia a cliente-->
            <label for="fecha" class="control-label">FECHA</label>
            <input type="text" name="fecha" class="form-control">
          </div>


          <div class="form-group">

            <button type="submit" class="btn btn-default"> <!--boton para registrar al cliente-->
              <i class="fa fa-plus"></i> registrar orden <!--registra al cliente en la fasede linea aerea-->
            </button>
          </div>
        </form>

      </div>
    </div>





  </div>

@endsection
