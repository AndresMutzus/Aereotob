@extends('layouts.adminlte')

@section('content')
  <div class="container">
    <!-- codigo mostrar clientes-->
    <div  class="col-md-12">  <!--definimos una fila que tiene 12 columnas-->
      <!--tabla-->
      <!-- para no usar llaves en el if usamos @ en blade php-->
      @if (count($orders) >0) <!-- si la cantidad de clientes es mayor a 0  mostramos lo siguinete-->

        <div class="panel panel-default">
          <div class="panel-heading">
            REPORTE -----LISTADO DE CLINETES----- <!-- imprimimos un listado llamado clientes-->
          </div>

          <div class="panel-body">
            <table class="table table-striped task-table">
              <thead>
                <!-- tabla con dos columnas clientes y accion-->
                <th>linea_aerea</th>
                <th>tipo_de_avion</th>
                <th>numero_de_vuelo</th>
                <th>matricula</th>
                <th>tiempo_inicio</th>
                <th>tiempo_terminado</th>
                <th>tiempo_total</th>
                <th>fecha</th>
                <th>Accion</th>
              </thead>
              <tbody>
                @foreach ($orders as $cliente) <!-- aqui vamos recorriendo el listado de los clintes-->
                  <tr> <!-- indica que los clientes seran mostrados por fila dentro del codigo foreach-->
                    <td  class="table-text"><div>{{ $cliente->linea_aerea}}</div></td> <!--desplegamos el nombre del cliente-->
                    <td  class="table-text"><div>{{ $cliente->tipo_de_avion}}</div></td>
                    <td  class="table-text"><div>{{ $cliente->numero_de_vuelo}}</div></td>
                    <td  class="table-text"><div>{{ $cliente->matricula}}</div></td>
                    <td  class="table-text"><div>{{ $cliente->tiempo_inicio}}</div></td>
                    <td  class="table-text"><div>{{ $cliente->tiempo_terminado}}</div></td>
                    <td  class="table-text"><div>{{ $cliente->tiempo_total}}</div></td>
                    <td  class="table-text"><div>{{ $cliente->fecha}}</div></td>


                    <td>
                      <!--este boton redirecciona a formulario en clientes editat balde php
                      <button type="submit" class="btn btn-success" onclick="location.href='cliente/{{ $cliente->id}}'"> <!-- creamos boton para actualizar
                      <i class="fa fa-pencil"></i>editar
                    </button> -->


                    <!-- aqui implementamos el codigo para borrar clientes ingresados-->
                    <form  action="{{ ('orden')}}/{{ $cliente->id}}" method="POST"> <!--hacemos referencia clientes en web.php-->
                      {{ csrf_field()}}
                      {{method_field('DELETE')}} <!-- aca eliminamos el cliente-->

                      <button type="submit" class="btn btn-danger"> <!-- creamos boton para borrar-->
                        <i class="fa fa-trash"></i>borrar
                      </button>
                    </form>
                  </td>
                </tr>
              @endforeach

            </tbody>

          </table>

        </div>

      </div>
    @endif <!-- termina la declaracion del if-->
  </div>
  </div>



@endsection
