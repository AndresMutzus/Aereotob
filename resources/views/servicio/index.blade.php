@extends('layouts.adminlte')

@section('content')
  <div class="container">
    <h1>Clientes</h1>
    @if(Session::has('message'))
     <div class="alert alert-success alert-dismissible" role="alert">
       <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
       {{Session::get('message')}}
     </div>
   @endif

   <table class='table'>
         <thead>
             <th>Nombre Servicio</th>
             <th>Tipo Servicio</th>

         </thead>

         @foreach($servicios as $servicio)
             <tbody>
                 <td>{{$servicio->nombre}}</td>
                 <td>{{$servicio->tipo}}</td>
             </tbody>
         @endforeach
     </table>
  </div>

@endsection
