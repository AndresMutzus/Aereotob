@extends('layouts.adminlte')
@section('content')
  <div class="container">

    <h1>Crear Servicio</h1>
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="panel panel-default">
            <div class="panel-heading">Crear Servicio</div>

            <div class="panel-body">
              <form class="form-horizontal" method="POST" action="{{ route('servicio.store') }}">
                {{ csrf_field() }}


                <div class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
                  <label for="nombre" class="col-md-4 control-label">Nombre Servicio</label>

                  <div class="col-md-6">
                    <input id="nombre" type="text" class="form-control" name="nombre" value="{{ old('nombre') }}" required autofocus>

                    @if ($errors->has('nombre'))
                      <span class="help-block">
                        <strong>{{ $errors->first('nombre') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>

                <div class="form-group{{ $errors->has('tipo') ? ' has-error' : '' }}">
                  <label for="tipo" class="col-md-4 control-label">Tipo Servicio</label>

                  <div class="col-md-6">
                    <input id="tipo" type="text" class="form-control" name="tipo" value="{{ old('tipo') }}" required autofocus>

                    @if ($errors->has('tipo'))
                      <span class="help-block">
                        <strong>{{ $errors->first('tipo') }}</strong>
                      </span>
                    @endif
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                      Crear
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
