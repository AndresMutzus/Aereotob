<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    $orders = \App\Order::all();
    return view('orden.index',compact('orders'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    $clientes = \App\Cliente::all();
    return view('orden.create',compact('clientes'));
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    $validator = Validator::make($request->all(),[ /** validamos la informacion ingresada**/
      'linea_aerea' => 'required|max:25',
      'tipo_de_avion' => 'required|max:30',
      'numero_de_vuelo' => 'required|max:10',
      'matricula' => 'required|max:10',
      'tiempo_inicio' => 'required|max:10',
      'tiempo_terminado' => 'required|max:10',
      'tiempo_total' => 'required|max:10',
      'fecha' => 'required|max:10',

    ]);

    if ($validator->fails()) {
      return redirect('/clientes') /** en caso falle redireccionaremos para regresaar a la ruta clientes**/
      ->withInput()
      ->withErrors($validator);
    }

    $orden= new \App\Order;
    $orden->linea_aerea = $request->linea_aerea;
    $orden->tipo_de_avion = $request->tipo_de_avion;
    $orden->numero_de_vuelo = $request->numero_de_vuelo;
    $orden->matricula = $request->matricula;
    $orden->tiempo_inicio = $request->tiempo_inicio;
    $orden->tiempo_terminado = $request->tiempo_terminado;
    $orden->tiempo_total = $request->tiempo_total;
    $orden->fecha = $request->fecha;
    $orden->save();

    return redirect('/orden');

  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    //
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    //
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    //
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    \App\Order::findOrFail($id)->delete();
    return  redirect('/orden');
  }
}
