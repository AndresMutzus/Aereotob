<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
  protected $table='orders';

  protected $fillable = [
    'linea_aerea', 'tipo_de_avion', 'numero_vuelo','matricula','tiempo_inicio','tiempo_terminado','tiempo_total','fecha',
  ];
}
